import React from "react";
import Avatar from "@mui/material/Avatar";
import Rating from "@mui/material/Rating";
import Card from "@mui/material/Card";

export const Avis = (props) => {
  return (
    <Card sx={{ maxWidth: "auto", marginBottom: 5, padding: 4 }}>
      <div className=" flex flex-row mb-4">
        <Avatar>👨🏼‍💻</Avatar>
        <div className="flex-col ml-3">
          <p>{props.username}</p>
          <div className=" flex flex-row">
            <Rating
              name="half-rating-read"
              precision={0.25}
              value={props.rating}
              readOnly
            />

            <p>{props.rating}</p>
          </div>
        </div>
      </div>
      <p>{props.comment}</p>
    </Card>
  );
};
