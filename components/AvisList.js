import React from "react";
import { Avis } from "./Avis";

export const AvisList = (props) => {
  return (
    <div>
      {props.props.map((avis, index) => {
        return <Avis key={index} {...avis} />;
      })}
    </div>
  );
};
