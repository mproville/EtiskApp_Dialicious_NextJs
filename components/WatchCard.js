import React from "react";
import Image from "next/image";
import Link from "next/link";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";

export const WatchCard = (models) => {
  return (
    <Card sx={{ maxWidth: 345 }}>
      <Link href={`/${models.brand}/${models.mId}`}>
        <CardActionArea>
          <a href={`/${models.brand}/${models.mId}`}>
            <CardMedia
              image={models.imgSrc}
              component="img"
              height="140"
              className="justify-center"
              alt={models.nom}
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                {models.nom}
              </Typography>
              <p>{models.refNb}</p>
              <div></div>
              <p>{models.material}</p>
              <p>{models.size} mm</p>
              <p>{models.thickness} mm</p>
            </CardContent>
          </a>
        </CardActionArea>
      </Link>
    </Card>
  );
};
