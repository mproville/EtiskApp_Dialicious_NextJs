import Head from "next/head";

export const Metatags = ({
  title = "⏱ Dialicious - Le site de la référence pour vos montres de Luxes",
  description = "The Next Gen tripAdvisor for Watches",
  image = "https://fireship.io/courses/react-next-firebase/img/featured.png",
}) => {
  return (
    <Head>
      <title>{title}</title>
      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:image" content={image} />
      <meta
        name="description"
        content="Dialicious is a referencing website where watch owners gather providing comments on their last piece of collection."
      />
    </Head>
  );
};
