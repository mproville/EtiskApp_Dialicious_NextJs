export default function Loader({ show }) {
  return show ? <div className="animate-bounce">Loading</div> : null;
}
