import React from "react";
import Link from "next/link";
import Image from "next/image";

export const BrandCard = (props) => {
  return (
    <div className="flex flex-col max-w-sm rounded border-4">
      <Link href={`/${props.brand.id}`}>
        <div>
          <div className="justify-center m-auto w-40">
            <Image
              src={props.brand.imgSrc}
              alt="logo"
              width={150}
              height={80}
              layout="intrinsic"
              className="justify-center"
            />
          </div>
          <section>
            <h3>{props.brand.name}</h3>
            <p>{props.brand.country} 🇨🇭</p>
            <p>{props.brand.description}</p>
          </section>
        </div>
      </Link>
    </div>
  );
};
