import React from "react";
import Rating from "@mui/material/Rating";
import Slider from "@mui/material/Slider";

export const ratingComment = (props) => {
  if (props === 5) {
    return "Excellent";
  }
  if (props < 5) {
    return "Very good";
  }
  if (props < 4) {
    return "Good";
  }
  if (props < 3) {
    return "Fair";
  }
  if (props < 2) {
    return "Poor";
  }
};

export const AvisDashBoard = (props) => {
  let ratingTotal = 0;
  props.props.map((avi) => {
    ratingTotal += avi.rating;
  });
  const avgRating = ratingTotal / props.props.length;
  return (
    <div className="flex flex-row p-4">
      {/* Card with avg rating & rated comment */}
      <div className="rounded-xl border-0 bg-slate-500/30 w-fit p-4 pt-10 mr-5">
        {/* avg */}
        <div className="flex flex-row">
          <h1 className="text-5xl font-bold">{avgRating.toFixed(2)}</h1>
          <div>
            <p>{ratingComment(avgRating)}</p>
            <div className="flex flex-row">
              <Rating
                name="read-only"
                value={avgRating}
                readOnly
                precision={0.5}
              />
              <p>{props.props.length} avis</p>
            </div>
          </div>
        </div>
        {/* List of rated comment */}
        <div className="mt-6">
          <div>
            <Rating
              name="read-only"
              defaultValue={5}
              precision={0.5}
              readOnly
            />{" "}
            Emotion
          </div>
          <div>
            <Rating
              name="read-only"
              defaultValue={4}
              precision={0.5}
              readOnly
            />{" "}
            Design
          </div>
          <div>
            <Rating
              name="read-only"
              defaultValue={5}
              precision={0.5}
              readOnly
            />{" "}
            Precision
          </div>
          <div>
            <Rating
              name="read-only"
              defaultValue={4}
              precision={0.5}
              readOnly
            />
            Confort
          </div>
          <div>
            <Rating
              name="read-only"
              defaultValue={4}
              precision={0.5}
              readOnly
            />{" "}
            Robustesse
          </div>
          <div>
            <Rating
              name="read-only"
              defaultValue={5}
              precision={0.5}
              readOnly
            />{" "}
            Rapport Qualité Prix
          </div>
        </div>
      </div>
      {/*Dash With Sliders and Tags */}
      <div className="w-full">
        {/* Slider */}
        <div className="flex flex-col gap-4">
          <div className="flex flex-col">
            <p className="text-xl m-auto font-bold">
              Importance Dans Une Collection
            </p>
            <div className="flex flex-row justify-between">
              <p>Secondaire</p>

              <p>Principale</p>
            </div>
            <Slider defaultValue={100} aria-label="Disabled slider" disabled />
          </div>
          <div className="flex flex-col ">
            <p className="text-xl m-auto font-bold">Frequence A Etre Portee</p>
            <div className="flex flex-row justify-between">
              <p>Rarement</p>

              <p>Frequemment</p>
            </div>
            <Slider defaultValue={50} aria-label="Disabled slider" disabled />
          </div>
          <div className="flex flex-col ">
            <p className="text-xl m-auto font-bold">
              Motivation Principale a l'Achat
            </p>
            <div className="flex flex-row justify-between">
              <p>Plaisir</p>
              <p>Investissement</p>
            </div>
            <Slider defaultValue={75} aria-label="Disabled slider" disabled />
          </div>
        </div>
        {/* tags */}
        <div className="flex flex-row flex-wrap gap-4 justify-center mt-4">
          <p className="rounded-2xl bg-slate-500/30 w-fit p-2">Mythique</p>
          <p className="rounded-2xl bg-slate-500/30 w-fit p-2">Classique</p>
          <p className="rounded-2xl bg-slate-500/30 w-fit p-2">Sobre</p>
          <p className="rounded-2xl bg-slate-500/30 w-fit p-2">Robuste</p>
          <p className="rounded-2xl bg-slate-500/30 w-fit p-2">Sublime</p>
          <p className="rounded-2xl bg-slate-500/30 w-fit p-2">Passionante</p>
          <p className="rounded-2xl bg-slate-500/30 w-fit p-2">Genereuse</p>
          <p className="rounded-2xl bg-slate-500/30 w-fit p-2">Rigoureuse</p>
        </div>
      </div>
    </div>
  );
};
