import React from "react";
import Link from "next/link";
import { AiOutlineYoutube } from "react-icons/ai";
import { BsInstagram } from "react-icons/bs";
import { FiTwitter } from "react-icons/fi";

export const Footer = () => {
  return (
    <footer className="p-4">
      <div className="flex flex-row justify-between flex-wrap">
        <ul>
          <h2 className="text-xl font-bold">A Propos</h2>
          <li className="mt-4">A propos de Dialicious</li>
          <li>Equipe</li>
          <li>Investisseurs</li>
          <li>Jobs</li>
          <li>Contactez-nous</li>
        </ul>
        <ul className="text-center">
          <h2 className="text-xl font-bold">Decouvrir</h2>
          <li className="mt-4">Plateforme</li>
          <li>Avis Dialicous</li>
          <li>Marques</li>
          <li>Partenaire</li>
          <li>Aide</li>
        </ul>
        <ul className="text-center">
          <h2 className="text-xl font-bold">Dialicious for Business</h2>
          <li className="mt-4">Marques</li>
          <li>Distributeurs</li>
          <li>Medias</li>
        </ul>
        <ul>
          <h2 className="text-xl font-bold">Langues</h2>
          <li className="mt-4">Français</li>
          <li>Anglais</li>
        </ul>
      </div>
      <div className="flex flex-row mt-5 flex-wrap">
        <p className="mr-2">Conditions generales d'utilisations |</p>
        <p className="mr-2">Charte Editoriale |</p>
        <p className="mr-2">Politique de confidentialite |</p>
        <p className="mr-2">Polique des cookies</p>
      </div>
      <div className="flex flex-row justify-between mt-10 mb-10">
        <p>Copyright Dialicious 2019-2021</p>
        <div className="flex flex-row">
          <Link href="https://www.instagram.com/fr_dialicious/">
            <a target="_blank">
              <BsInstagram className="mr-2 text-5xl" />
            </a>
          </Link>
          <Link href="https://www.youtube.com/c/dialicious">
            <a target="_blank">
              <AiOutlineYoutube className="mr-2 text-5xl" />
            </a>
          </Link>
          <FiTwitter className="mr-2 text-5xl" />
        </div>
      </div>
    </footer>
  );
};
