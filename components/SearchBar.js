import React from "react";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";

export const SearchBar = ({ inputHandler }) => {
  return (
    <Box
      component="form"
      sx={{
        maxWidth: 650,
      }}
      noValidate
      autoComplete="off"
      className="m-auto mb-4"
    >
      <TextField
        fullWidth
        label="Looking for a Watch ? Search here"
        id="Search"
        onChange={inputHandler}
      />
    </Box>
  );
};
