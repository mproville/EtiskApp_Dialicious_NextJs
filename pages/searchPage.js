import React from "react";
import { useState } from "react";
import TextField from "@mui/material/TextField";
import { SearchBar } from "../components/SearchBar";
import { firestore } from "../lib/firebase";
import { CardActionArea } from "@mui/material";

import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import { CardContent } from "@mui/material";
import Typography from "@mui/material/Typography";

export async function getServerSideProps() {
  const data = await firestore.collection("marques").get();
  const data2 = await firestore.collectionGroup("model").get();
  const marques = data.docs.map((doc) => doc.data());
  const model = data2.docs.map((doc) => doc.data());
  const mainData = marques.concat(model);

  return {
    props: { mainData },
  };
}

const searchPage = (props) => {
  const [search, setSearch] = useState("");
  const data = props.mainData;

  let inputHandler = (e) => {
    let lowercase = e.target.value.toLowerCase();
    setSearch(lowercase);
  };

  const filteredWatch = data.filter((watch) => {
    if (watch.nom.toLowerCase().includes(search)) {
      return watch;
    }
  });

  const filterData = filteredWatch.map((watch) => {
    return watch;
  });

  return (
    <>
      <div className="mt-10">
        <SearchBar inputHandler={inputHandler} />
        {filterData.map((watch, index) => {
          return (
            <>
              <Card sx={{ minWidth: 275 }}>
                <CardActionArea>
                  <CardContent>
                    <Typography key={index} className="pl-4">
                      {watch.nom.toLowerCase()}
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </>
          );
        })}
      </div>
    </>
  );
};

export default searchPage;
