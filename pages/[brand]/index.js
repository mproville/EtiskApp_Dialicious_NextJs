import React from "react";
import Image from "next/image";
import { useState } from "react";
import { postToJSON } from "../../lib/firebase";
import { firestore } from "../../lib/firebase";

import { WatchCard } from "../../components/WatchCard";
import { AvisList } from "../../components/AvisList";
import { AvisDashBoard } from "../../components/AvisDashBoard";
import { BreadCrumb } from "../../components/BreadCrumb";
import Loader from "../../components/Loader";

import Button from "@mui/material/Button";
import Rating from "@mui/material/Rating";

const LIMIT = 5;

export async function getServerSideProps({ params }) {
  const data = await firestore
    .collection("marques")
    .doc(params.brand)
    .collection("model")
    .limit(LIMIT)
    .get();
  const brandQuery = await firestore
    .collection("marques")
    .where("id", "==", params.brand)
    .get();

  const postsQuery = firestore
    .collectionGroup("avis")
    .where("brandId", "==", params.brand)
    .limit(LIMIT);

  const ttAvisQuery = firestore
    .collectionGroup("avis")
    .where("brandId", "==", params.brand);

  const brand = brandQuery.docs.map((doc) => doc.data());
  const models = data.docs.map((doc) => doc.data());
  const avi = (await postsQuery.get()).docs.map(postToJSON);
  const ttAvis = (await ttAvisQuery.get()).docs.map(postToJSON);

  return {
    props: { models, avi, params, ttAvis, brand },
    // revalidate: 5000,
  };
}

// export async function getStaticPaths() {
//   return {
//     paths: [{ params: { brand: "1" } }],
//     fallback: "blocking",
//   };
// }

const Index = (props) => {
  console.log(props);
  const [posts, setPosts] = useState(props.avi);
  const [loading, setLoading] = useState(false);
  const [postsEnd, setPostsEnd] = useState(false);
  let ratingTotal = 0;

  const getMorePosts = async () => {
    setLoading(true);
    const last = posts[posts.length - 1];
    const query = firestore
      .collectionGroup("avis")
      .where("brandId", "==", props.params.brand)
      .limit(LIMIT);
    const newPosts = (await query.get()).docs.map((doc) => doc.data());
    setPosts(posts.concat(newPosts));
    setLoading(false);
    if (newPosts.length < LIMIT) {
      setPostsEnd(true);
    }
  };

  props.ttAvis.map((avi) => {
    ratingTotal += avi.rating;
  });
  const avgRating = ratingTotal / props.ttAvis.length;

  return (
    <>
      <div className="flex flex-row justify-between">
        <h1 className="p-4 text-3xl">
          {props.brand[0].nom}, {props.brand[0].country}
        </h1>
        <Image
          src={props.brand[0].imgSrc}
          alt="logo"
          width={320}
          height={75}
          layout="fixed"
          className="justify-center"
        />
      </div>
      <div className="p-4 flex flex-row gap-4 mt-10">
        <Rating name="read-only" value={avgRating} readOnly precision={0.5} />
        <p>
          {props.ttAvis.length} avis sur {props.models.length} references
        </p>
      </div>
      {/* <BreadCrumb brand={props.brand} /> */}
      <div className="flex flex-row flex-wrap gap-8 p-4 m-auto">
        {props.models.map((model, index) => {
          return <WatchCard key={index} {...model} {...props.params} />;
        })}
      </div>
      <p className="p-4">{props.brand[0].description}</p>
      <AvisDashBoard props={props.ttAvis} />
      <div className="mb-10">
        <AvisList props={posts} />
      </div>
      <Loader show={loading} />
      {props.avi.length > 0 ? (
        <>
          {" "}
          {!loading && !postsEnd && (
            <Button
              onClick={getMorePosts}
              variant="outlined"
              size="medium"
              className="mb-10"
              color="primary"
            >
              Load More
            </Button>
          )}{" "}
        </>
      ) : (
        <></>
      )}
    </>
  );
};

export default Index;
