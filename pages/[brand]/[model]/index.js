import React from "react";
import Image from "next/image";
import { firestore } from "../../../lib/firebase";
import { AvisList } from "../../../components/AvisList";
import { AvisDashBoard } from "../../../components/AvisDashBoard";

export async function getServerSideProps({ params }) {
  const data = await firestore
    .collection("marques")
    .doc(params.brand)
    .collection("model")
    .where("mId", "==", params.model)
    .get();
  const model = data.docs.map((doc) => doc.data());

  const avis = await firestore
    .collectionGroup("avis")
    .where("modelId", "==", params.model)
    .get();
  const avi = avis.docs.map((doc) => doc.data());

  return {
    props: { model, avi },
    // revalidate: 5000,
  };
}

// export async function getStaticPaths() {
//   return {
//     paths: [{ params: { brand: "1", model: "2" } }],
//     fallback: "blocking",
//   };
// }

const index = (props) => {
  const imgSrc = props.model[0].imgSrc;
  return (
    <>
      <div className="max-w-xs max-h-80 mb-20 m-auto">
        <Image
          src={imgSrc}
          alt="logo"
          width={320}
          height={400}
          layout="fixed"
        />
      </div>
      <AvisDashBoard props={props.avi} />
      <AvisList props={props.avi} />
    </>
  );
};

export default index;
