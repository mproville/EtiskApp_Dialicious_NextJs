import "../styles/globals.css";
import { Footer } from "../components/Footer";
import { Metatags } from "../components/Metatags";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Metatags />
      <Component {...pageProps} />
      <Footer />
    </>
  );
}

export default MyApp;
