import Image from "next/image";
import styles from "../styles/Home.module.css";
import { Footer } from "../components/Footer";
import { firestore } from "../lib/firebase";
import { BrandCard } from "../components/BrandCard";
import { Metatags } from "../components/Metatags";

export async function getStaticProps() {
  const data = await firestore.collection("marques").get();
  const marques = data.docs.map((doc) => doc.data());
  return {
    props: { marques },
  };
}

export default function Home(props) {
  const marques = props.marques;
  return (
    <main>
      <section className="flex flex-row flex-wrap gap-10">
        {marques.map((marque, index) => {
          return <BrandCard key={index} brand={marque} />;
        })}
      </section>
    </main>
  );
}
