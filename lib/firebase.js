import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";
import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyCHPFaQH1LbwDWToO_JbT3pUTFP4aLh2Uc",
  authDomain: "dialicious-fae4f.firebaseapp.com",
  projectId: "dialicious-fae4f",
  storageBucket: "dialicious-fae4f.appspot.com",
  messagingSenderId: "292054657084",
  appId: "1:292054657084:web:0eb834a89ffd85aead7b39",
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export const auth = firebase.auth();
export const firestore = firebase.firestore();
export const storage = firebase.storage();
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

export async function getModelWithBrand(brand) {
  const usersRef = firestore.collection("users");
  const query = usersRef.where("username", "==", username).limit(1);
  const userDoc = (await query.get()).docs[0];
  return userDoc;
}

export function postToJSON(doc) {
  const data = doc.data();
  return {
    ...data,
  };
}
