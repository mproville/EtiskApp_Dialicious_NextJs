/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: [
      "upload.wikimedia.org",
      "www.audemarspiguet.com",
      "jaeger-lecoultre.rokka.io",
      "content.rolex.com",
      "www.omegawatches.com",
    ],
  },
};

module.exports = nextConfig;
